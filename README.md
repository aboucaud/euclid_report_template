# Euclid internal report template in LaTeX

Internal report template that emulates the *Microsoft Word* report template and lets you write your document in LaTeX.

**Author:** Yannick Copin (<y.copin@ipnl.in2p3.fr>)

## Preview

The top front page output will look like:
![report preview](_static/front-preview.png)

A full preview of the example document can seen [here](_static/interneuclid_example.pdf)

## Usage

### Getting started

A `interneuclid_example.tex` file is provided with the project.

Modify it to your convenience.

Compile document **using XeLaTeX** instead of pdfLaTeX.

### General

Write the LaTeX document using the provided euclid class `interneuclid.cls`

    \documentclass{interneuclid}


**Note:** one might need to include the shebang line on top of the .tex file

    %!TEX program = xelatex
